default:
  interruptible: true
  image: registry.gitlab.com/isekai/infrastructure/dockerfile/rust:1.60.0-main
  retry:
    max: 2
    when: runner_system_failure

stages:
  - test
  - pre-publish
  - publish

cache:
  key: $CI_COMMIT_REF_SLUG-$CI_JOB_NAME
  paths:
    - target

####################################################################################################

test:
  stage: test
  script:
    - mkdir ./reports/
    - cargo test -- -Z unstable-options --format json --report-time | cargo2junit > ./reports/junit.xml
  artifacts:
    reports:
      junit: reports/junit.xml

lint:
  stage: test
  script:
    - cargo clippy &> >(tee lint.txt)
    - test $(grep --count '^warning' lint.txt) -eq 0

coverage:
  stage: test
  script:
    - cargo tarpaulin --verbose --out Xml --output-dir ./reports/
  coverage: /^\d+.\d+% coverage/
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: reports/cobertura.xml

format:
  stage: test
  script:
    - cargo fmt -- --check

msrv:
  stage: test
  script:
    - cargo msrv verify

package:
  stage: test
  script:
    - cargo package

####################################################################################################

version:
  stage: pre-publish
  script:
    - version=v$(grep -Eo '^version = "[^"]*"$' Cargo.toml  | sed -E 's|.+ = "([^"]+)"$|\1|')
    - test $version = $CI_COMMIT_TAG
  rules:
    - if: $CI_COMMIT_TAG
      when: on_success

####################################################################################################

publish:
  stage: publish
  script:
    - cargo login $CRATES_IO_TOKEN
    - cargo publish
  rules:
    - if: $CI_COMMIT_TAG
      when: on_success

release:
  stage: publish
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  script:
    - echo "Creating release..."
  release:
    tag_name: $CI_COMMIT_TAG
    description: "./changelog/$CI_COMMIT_TAG.md"
  rules:
    - if: $CI_COMMIT_TAG
      when: on_success
