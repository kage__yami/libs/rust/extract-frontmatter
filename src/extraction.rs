pub fn extract_by_line_index(input: &str, index: usize) -> (&str, &str) {
    if index == 0 {
        return (&input[0..0], input);
    }

    let mut newlines_seen = 0;

    match input.find(|c| {
        if c == '\n' {
            newlines_seen += 1;
        }

        newlines_seen >= index
    }) {
        None => (input, &input[input.len()..]),
        Some(i) => (&input[..=i], &input[(i + 1)..]),
    }
}

pub fn extract_by_delimiter_line<'input>(input: &'input str, delim: &str) -> (&'input str, &'input str) {
    let delim = delim.trim();

    let (meta, data) = extract_by_line_index(
        input,
        match input.lines().enumerate().find(|(_, line)| line.trim() == delim) {
            None => {
                return (input, &input[input.len()..]);
            }
            Some((index, _)) => index + 1,
        },
    );

    (strip_meta_suffix(meta, delim), data)
}

pub fn extract_by_line_prefix<'input>(input: &'input str, prefix: &str) -> (&'input str, &'input str) {
    extract_by_line_index(
        input,
        match input.lines().enumerate().find(|(_, line)| !line.starts_with(prefix)) {
            None => {
                return (input, &input[input.len()..]);
            }
            Some((index, _)) => index,
        },
    )
}

pub fn extract_by_enclosing_lines<'input>(input: &'input str, delim: &str) -> (&'input str, &'input str) {
    let delim = delim.trim();

    let mut found_first_delim = false;

    let (meta, data) = extract_by_line_index(
        input,
        match input.lines().enumerate().find(|(_, line)| {
            if line.trim() == delim {
                if found_first_delim {
                    return true;
                }

                found_first_delim = true;
            }

            false
        }) {
            None => {
                return (strip_meta_prefix(input, delim), &input[input.len()..]);
            }
            Some((index, _)) => index + 1,
        },
    );

    (strip_meta_prefix(strip_meta_suffix(meta, delim), delim), data)
}

fn strip_meta_suffix<'meta>(meta: &'meta str, suffix: &str) -> &'meta str {
    meta.strip_suffix(&format!("{suffix}\r\n")).unwrap_or_else(|| {
        meta.strip_suffix(&format!("{suffix}\n")).unwrap_or_else(|| meta.strip_suffix(suffix).unwrap_or(meta))
    })
}

fn strip_meta_prefix<'meta>(meta: &'meta str, prefix: &str) -> &'meta str {
    meta.strip_prefix(&format!("{prefix}\r\n")).unwrap_or_else(|| {
        meta.strip_prefix(&format!("{prefix}\n")).unwrap_or_else(|| meta.strip_prefix(prefix).unwrap_or(meta))
    })
}
