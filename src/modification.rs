// intersperse()->fold() would be preferred (rather than collect()->join()), but intersperse() is nightly-only at the
// time of writing (Rust 1.60.0): https://doc.rust-lang.org/std/iter/trait.Iterator.html#method.intersperse
// ----------------------------------------------------------------------------------------------------
// input.lines().map(|line| line.strip_prefix(prefix).unwrap_or(line)).intersperse("\n").fold(
//     String::with_capacity(input.len() - prefix.len() * input.lines().count()),
//     |mut acc, line| {
//         acc.push_str(line);
//         acc
//     },
// )

pub fn strip_prefix(meta: &str, prefix: &str) -> String {
    meta.lines().map(|line| line.strip_prefix(prefix).unwrap_or(line)).collect::<Vec<_>>().join("\n")
}

pub fn trim_whitespace(meta: &str) -> String {
    meta.lines().map(str::trim).collect::<Vec<_>>().join("\n")
}

pub fn strip_first_line(meta: &str) -> String {
    String::from(
        &meta[match meta.find('\n') {
            None => meta.len(),
            Some(index) => index + 1,
        }..],
    )
}

pub fn strip_last_line(meta: &str) -> String {
    String::from(
        &meta[..match meta.rfind('\n') {
            None => meta.len(),
            Some(index) => index + 1,
        }],
    )
}
