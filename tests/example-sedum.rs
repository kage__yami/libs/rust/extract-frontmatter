#![warn(clippy::cargo, clippy::nursery, clippy::pedantic)]

use extract_frontmatter::config::{Modifier, Splitter};
use extract_frontmatter::Extractor;

/// Ensures that new code equivalent to old `v2.1.0` code now misses the front-matter, as the old code is considered
/// buggy (due to it being "unexpected"). This code is the same logic that [Sedum previously had].
///
/// [Sedum previously had]: https://github.com/ellygaytor/Sedum/blob/main/src/generation.rs#L74
#[test]
fn missing_frontmatter() {
    let (settings_yaml, markdown_content) = Extractor::new(Splitter::DelimiterLine("---"))
        .with_modifier(Modifier::StripFirstLine)
        .extract(include_str!("../resources/tests/third-party/sedum/raw.md"));

    assert_eq!(
        (settings_yaml.trim(), markdown_content.trim()),
        (
            include_str!("../resources/tests/third-party/sedum/old/meta.yml").trim(),
            include_str!("../resources/tests/third-party/sedum/old/data.md").trim()
        )
    );
}

/// Tests the new functionality necessary for Sedum to continue using the library in the same way as before.
#[test]
fn with_frontmatter() {
    let (settings_yaml, markdown_content) = Extractor::new(Splitter::EnclosingLines("---"))
        .extract(include_str!("../resources/tests/third-party/sedum/raw.md"));

    assert_eq!(
        (settings_yaml.trim(), markdown_content.trim()),
        (
            include_str!("../resources/tests/third-party/sedum/new/meta.yml").trim(),
            include_str!("../resources/tests/third-party/sedum/new/data.md").trim()
        )
    );
}
