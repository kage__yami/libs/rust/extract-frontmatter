#![warn(clippy::cargo, clippy::nursery, clippy::pedantic)]

use extract_frontmatter::config::{Modifier, Splitter};
use extract_frontmatter::Extractor;
use std::borrow::Cow;

#[test]
fn no_matching_lines() {
    let (actual, _) = Extractor::new(Splitter::LineIndex(3))
        .with_modifier(Modifier::StripPrefix("// "))
        .extract(concat!("Front-matter line 1\n", "Front-matter line 2\n", "Front-matter line 3"));

    let expected: Cow<str> =
        Cow::Owned(String::from(concat!("Front-matter line 1\n", "Front-matter line 2\n", "Front-matter line 3")));

    assert_eq!(actual, expected);
}

#[test]
fn matching_lines() {
    let (actual, _) = Extractor::new(Splitter::LineIndex(3))
        .with_modifier(Modifier::StripPrefix("// "))
        .extract(concat!("// Front-matter line 1\n", "// Front-matter line 2\n", "// Front-matter line 3"));

    let expected: Cow<str> =
        Cow::Owned(String::from(concat!("Front-matter line 1\n", "Front-matter line 2\n", "Front-matter line 3")));

    assert_eq!(actual, expected);
}

#[test]
fn matching_lines_multiple_steps() {
    let (actual, _) = Extractor::new(Splitter::LineIndex(3))
        .with_modifier(Modifier::StripPrefix("// "))
        .with_modifier(Modifier::StripPrefix("Front-matter "))
        .with_modifier(Modifier::StripPrefix("line "))
        .extract(concat!("// Front-matter line 1\n", "// Front-matter line 2\n", "// Front-matter line 3"));

    let expected: Cow<str> = Cow::Owned(String::from(concat!("1\n", "2\n", "3")));

    assert_eq!(actual, expected);
}
