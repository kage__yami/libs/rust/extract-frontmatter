#![warn(clippy::cargo, clippy::nursery, clippy::pedantic)]

use extract_frontmatter::config::Splitter;
use extract_frontmatter::Extractor;
use std::borrow::Cow;

#[test]
fn zero_lines_empty_prefix() {
    assert_eq!(Extractor::new(Splitter::LinePrefix("")).extract(""), (Cow::Borrowed(""), ""));
}

#[test]
fn zero_lines_missing_prefix() {
    assert_eq!(Extractor::new(Splitter::LinePrefix("// ")).extract(""), (Cow::Borrowed(""), ""));
}

#[test]
fn one_line_empty_prefix() {
    assert_eq!(
        Extractor::new(Splitter::LinePrefix("")).extract("// Front-matter line"),
        (Cow::Borrowed("// Front-matter line"), "")
    );
}

#[test]
fn one_line_missing_prefix() {
    assert_eq!(Extractor::new(Splitter::LinePrefix("// ")).extract("Data line"), (Cow::Borrowed(""), "Data line"));
}

#[test]
fn one_line_prefix_first() {
    assert_eq!(
        Extractor::new(Splitter::LinePrefix("// ")).extract("// Front-matter line"),
        (Cow::Borrowed("// Front-matter line"), "")
    );
}

#[test]
fn two_lines_empty_prefix() {
    assert_eq!(
        Extractor::new(Splitter::LinePrefix("")).extract(concat!("// Front-matter line 1\n", "// Front-matter line 2")),
        (Cow::Borrowed(concat!("// Front-matter line 1\n", "// Front-matter line 2")), "")
    );
}

#[test]
fn two_lines_missing_prefix() {
    assert_eq!(
        Extractor::new(Splitter::LinePrefix("// ")).extract(concat!("Data line 1\n", "Data line 2")),
        (Cow::Borrowed(""), concat!("Data line 1\n", "Data line 2"))
    );
}

#[test]
fn two_lines_prefix_first() {
    assert_eq!(
        Extractor::new(Splitter::LinePrefix("// ")).extract(concat!("// Front-matter line\n", "Data line")),
        (Cow::Borrowed("// Front-matter line\n"), "Data line")
    );
}

#[test]
fn two_lines_prefix_second() {
    assert_eq!(
        Extractor::new(Splitter::LinePrefix("// ")).extract(concat!("Data line 1\n", "// Data line 2")),
        (Cow::Borrowed(""), concat!("Data line 1\n", "// Data line 2"))
    );
}

#[test]
fn two_lines_prefix_first_and_second() {
    assert_eq!(
        Extractor::new(Splitter::LinePrefix("// "))
            .extract(concat!("// Front-matter line 1\n", "// Front-matter line 2")),
        (Cow::Borrowed(concat!("// Front-matter line 1\n", "// Front-matter line 2")), "")
    );
}

#[test]
fn three_lines_empty_prefix() {
    assert_eq!(
        Extractor::new(Splitter::LinePrefix("")).extract(concat!(
            "// Front-matter line 1\n",
            "// Front-matter line 2\n",
            "// Front-matter line 3"
        )),
        (Cow::Borrowed(concat!("// Front-matter line 1\n", "// Front-matter line 2\n", "// Front-matter line 3")), "")
    );
}

#[test]
fn three_lines_missing_prefix() {
    assert_eq!(
        Extractor::new(Splitter::LinePrefix("// ")).extract(concat!("Data line 1\n", "Data line 2\n", "Data line 3")),
        (Cow::Borrowed(""), concat!("Data line 1\n", "Data line 2\n", "Data line 3"))
    );
}

#[test]
fn three_lines_prefix_first() {
    assert_eq!(
        Extractor::new(Splitter::LinePrefix("// ")).extract(concat!(
            "// Front-matter line\n",
            "Data line 1\n",
            "Data line 2"
        )),
        (Cow::Borrowed("// Front-matter line\n"), concat!("Data line 1\n", "Data line 2"))
    );
}

#[test]
fn three_lines_prefix_second() {
    assert_eq!(
        Extractor::new(Splitter::LinePrefix("// ")).extract(concat!(
            "Data line 1\n",
            "// Data line 2\n",
            "Data line 3"
        )),
        (Cow::Borrowed(""), concat!("Data line 1\n", "// Data line 2\n", "Data line 3"))
    );
}

#[test]
fn three_lines_prefix_third() {
    assert_eq!(
        Extractor::new(Splitter::LinePrefix("// ")).extract(concat!(
            "Data line 1\n",
            "Data line 2\n",
            "// Data line 3"
        )),
        (Cow::Borrowed(""), concat!("Data line 1\n", "Data line 2\n", "// Data line 3"))
    );
}

#[test]
fn three_lines_prefix_first_and_second() {
    assert_eq!(
        Extractor::new(Splitter::LinePrefix("// ")).extract(concat!(
            "// Front-matter line 1\n",
            "// Front-matter line 2\n",
            "Data line 1"
        )),
        (Cow::Borrowed(concat!("// Front-matter line 1\n", "// Front-matter line 2\n")), "Data line 1")
    );
}

#[test]
fn three_lines_prefix_first_and_third() {
    assert_eq!(
        Extractor::new(Splitter::LinePrefix("// ")).extract(concat!(
            "// Front-matter line\n",
            "Data line 1\n",
            "// Data line 2"
        )),
        (Cow::Borrowed("// Front-matter line\n"), concat!("Data line 1\n", "// Data line 2"))
    );
}

#[test]
fn three_lines_prefix_second_and_third() {
    assert_eq!(
        Extractor::new(Splitter::LinePrefix("// ")).extract(concat!(
            "Data line 1\n",
            "// Data line 2\n",
            "// Data line 3"
        )),
        (Cow::Borrowed(""), concat!("Data line 1\n", "// Data line 2\n", "// Data line 3"))
    );
}

#[test]
fn three_lines_prefix_first_second_and_third() {
    assert_eq!(
        Extractor::new(Splitter::LinePrefix("// ")).extract(concat!(
            "// Front-matter line 1\n",
            "// Front-matter line 2\n",
            "// Front-matter line 3"
        )),
        (Cow::Borrowed(concat!("// Front-matter line 1\n", "// Front-matter line 2\n", "// Front-matter line 3")), "")
    );
}
